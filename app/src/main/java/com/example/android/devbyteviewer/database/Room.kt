/*
 * Copyright 2018, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.example.android.devbyteviewer.database

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.*

// 4. Define a @Dao interface called VideoDao
@Dao
interface VideoDao {
    // 5. Add getVideos Query function that returns a List of DatabaseVideo
    @Query("SELECT * FROM databasevideo")
    fun getVideos(): LiveData<List<DatabaseVideo>>

    // 6. Add an Insert function called insertAll that takes vararg DatabaseVideo.  This is an upsert, so include a conflict strategy
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg videos: DatabaseVideo)
}

// 7. Create an abstract VideosDatabase class that extends RoomDatabase and annotate it with @Database including entities and version
@Database(entities = [DatabaseVideo::class], version = 1)
abstract class VideosDatabase: RoomDatabase() {
    // 8. add the Dao
    abstract val videoDao: VideoDao
}

// 9. Create a variable using the singleton pattern
private lateinit var INSTANCE: VideosDatabase

// 10. Define a getDatabase function to return the VideosDatabase
fun getDatabase(context: Context): VideosDatabase {
    // 11. Check whether the database has been initialized
    // 12. Make sure the code is synchronized so that it's thread safe
    synchronized(VideosDatabase::class.java) {
        if (!::INSTANCE.isInitialized) {
            INSTANCE = Room.databaseBuilder(context.applicationContext, VideosDatabase::class.java, "videos").build()
        }
    }
    return INSTANCE
}
